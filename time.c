//Converting time into minutes
#include <stdio.h>

int main()
{
    int hr,min,res;
	printf("Enter time in hours and minutes \n");
    scanf("%d %d",&hr,&min);
    res=(hr*60)+min;
    printf("Time in minutes is %d",res);
	return 0;
}
//Converting time into seconds
#include <stdio.h>

int main()
{
    int hr,min,sec,res;
	printf("Enter time in hours,minutes and seconds \n");
    scanf("%d %d %d",&hr,&min,&sec);
    res=(hr*3600)+(min*60)+sec;
    printf("Time in seconds is %d",res);
	return 0;
}
//Additonal programs

#include <stdio.h>
#include <math.h>
int add(int a,int b)
{
int f=a+b;
return f;
}
float sum(float a,float b)
{
float g=a+b;
return g;
}
float square(float a)
{
float sq=pow(a,2);
return sq;    
}
int main()
{
    int a,b;
    float c,d,e;
    printf("Enter two integer numbers \n");
    scanf("%d %d",&a,&b);
    printf("The addition of these two integer numbers is %d",add(a,b));
    printf("\n Enter two decimal numbers \n");
    scanf("%f %f",&c,&d);
    printf("The addition of these two decimal numbers is %.2f",sum(c,d));
    printf("\nEnter a number \n");
    scanf("%f",&e);
    printf("The square of the given number is %.2f",square(e));
	return 0;
}
